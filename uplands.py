#!/usr/bin/python3

import requests
from bs4 import BeautifulSoup
import json
from xml.sax.saxutils import escape
import simplekml

#Set filename/path for KML file output
kmlfile = "uplands.kml"
#Set KML schema name
kmlschemaname = "uplands"
#Set page URL
pageURL = "https://uplandscheese.com/where-to-buy-our-cheese/"
#Start a session with the given page URL
session = requests.Session()
page = session.get(pageURL)
#Soupify the HTML
soup = BeautifulSoup(page.content, 'html.parser')
#Specify the 16th instance of the script tag to get the locations
locationshtml = soup.findAll('script')[15]
#Shed off the JS to get down to the pure JSON and create a JSON object
locationsjson = json.loads(locationshtml.get_text()[55:-5])

#Initialize kml object
kml = simplekml.Kml()
#Add schema, which is required for a custom address field
schema = kml.newschema(name=kmlschemaname)
schema.newsimplefield(name="address",type="string")

#Iterate through JSON for each location
for location in locationsjson['places']:
    #Map the JSON attributes to kml fields
    lat = location['location']['lat']
    lng = location['location']['lng']
    #...and escape characters like "&" into valid xml ("&amp;")
    locationname = escape(location['content'])
    locationaddress = escape(location['address'])
    #Create the point name and description in the kml
    point = kml.newpoint(name=locationname,description="Uplands Cheese")
    #Then, add the custom "SimpleData" address field to kml file with proper hierarchical kml data structure
    point.extendeddata.schemadata.schemaurl = kmlschemaname
    simpledata = point.extendeddata.schemadata.newsimpledata("address",locationaddress)
    #Finally, add coordinates to the feature
    point.coords=[(lng,lat)]
#Save the final KML file
kml.save(kmlfile)
