# Uplands Cheese Map
Extracts Uplands Cheese retailer locations from the official website, and places them into KML format.

## Dependencies
* Python 3.x
    * Requests module for session functionality
    * Beautiful Soup 4.x (bs4) for scraping
    * JSON module for parsing JSON-based geodata
    * Xml.sax.saxutils for conveniently formatting utf8 characters into valid XML/KML
    * Simplekml for easily building KML files
* Also of course depends on official [Uplands Cheese website](https://uplandscheese.com/where-to-buy-our-cheese/).
